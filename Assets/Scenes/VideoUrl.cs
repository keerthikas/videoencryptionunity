﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.IO;
using UnityEngine.Video;
using UnityEngine.UI;

class VideoUrl : MonoBehaviour
{

    public Text LoadTxt;
    void Start()
    {
        StartCoroutine(GetText());
    }

    IEnumerator GetText()
    {
        Debug.Log("Get Text");
        WWW www = new WWW("https://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        while (!www.isDone)
        {
            Debug.Log("downloaded " + (www.progress * 100).ToString() + "%...");
            LoadTxt.text = "downloaded " + (www.progress * 100).ToString() + "%...";
            yield return null;
        }
        LoadTxt.text = "Download Completed now run play scence";
              var videname = "video1.dat";
        var path = Application.streamingAssetsPath + "/video1.dat"; //Application.temporaryCachePath+ "/video1.mp4";
        

        Debug.Log(path);
#if UNITY_EDITOR
        var dbPath = string.Format(@"Assets/StreamingAssets/{0}", videname);
#else
        // check if file exists in Application.persistentDataPath
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, videname);

        if (!File.Exists(filepath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + videname);  // this is the path to your StreamingAssets in android
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
                 var loadDb = Application.dataPath + "/Raw/" + videname;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);
#elif UNITY_WP8
                var loadDb = Application.dataPath + "/StreamingAssets/" + videname;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);

#elif UNITY_WINRT
		var loadDb = Application.dataPath + "/StreamingAssets/" + videname;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
		
#elif UNITY_STANDALONE_OSX
		var loadDb = Application.dataPath + "/Resources/Data/StreamingAssets/" + videname;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
#else
	var loadDb = Application.dataPath + "/StreamingAssets/" + videname;  // this is the path to your StreamingAssets in iOS
	// then save to Application.persistentDataPath
	File.Copy(loadDb, filepath);

#endif

            Debug.Log("Database written");
        }

        var dbPath = filepath;
#endif
         File.WriteAllBytes(dbPath, www.bytes);

            
        Debug.Log(www.bytes);
    }
   
}