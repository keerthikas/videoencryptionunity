﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class main : MonoBehaviour {
    public Button play, download;
	// Use this for initialization
	void Start () {
        play.onClick.AddListener(playscence);
        download.onClick.AddListener(downloadscence);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void playscence()
    {
        SceneManager.LoadScene("playvideo");
    }
    void downloadscence()
    {
        SceneManager.LoadScene("downloadvideo");

    }
   
}
