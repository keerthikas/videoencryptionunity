﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.IO;
using UnityEngine.Video;

class play : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    public AudioSource audioSource;
    string dbPaths;
    void Start()
    {
        GetText();
    }

    void GetText()
    {
       /* Debug.Log("Get Text");
        WWW www = new WWW("https://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        while (!www.isDone)
        {
            Debug.Log("downloaded " + (www.progress * 100).ToString() + "%...");
            yield return null;
        }*/

        var videname = "video1.mp4";
        var videname1 = "video1.dat";

        var path = Application.streamingAssetsPath + "/video1.mp4"; //Application.temporaryCachePath+ "/video1.mp4";
        var path1 = Application.streamingAssetsPath + "/video1.dat"; //Application.temporaryCachePath+ "/video1.mp4";


        Debug.Log(path);
#if UNITY_EDITOR
        var dbPath = string.Format(@"Assets/StreamingAssets/{0}", videname);
        var dbPath1 = string.Format(@"Assets/StreamingAssets/{0}", videname1);

#else
        // check if file exists in Application.persistentDataPath
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, videname);
        var filepath1 = string.Format("{0}/{1}", Application.persistentDataPath, videname1);

        if (!File.Exists(filepath))
        {
            Debug.Log("Database not in Persistent path");
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + videname);  // this is the path to your StreamingAssets in android
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
         var loadDb1 = new WWW("jar:file://" + Application.dataPath + "!/assets/" + videname1);  // this is the path to your StreamingAssets in android
            while (!loadDb1.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath1, loadDb1.bytes);
#elif UNITY_IOS
                 var loadDb = Application.dataPath + "/Raw/" + videname;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);
        var loadDb1 = Application.dataPath + "/Raw/" + videname1;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb1, filepath1);
#elif UNITY_WP8
                var loadDb = Application.dataPath + "/StreamingAssets/" + videname;  // this is the path to your StreamingAssets in iOS
                // then save to Application.persistentDataPath
                File.Copy(loadDb, filepath);

#elif UNITY_WINRT
		var loadDb = Application.dataPath + "/StreamingAssets/" + videname;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
		
#elif UNITY_STANDALONE_OSX
		var loadDb = Application.dataPath + "/Resources/Data/StreamingAssets/" + videname;  // this is the path to your StreamingAssets in iOS
		// then save to Application.persistentDataPath
		File.Copy(loadDb, filepath);
#else
	var loadDb = Application.dataPath + "/StreamingAssets/" + videname;  // this is the path to your StreamingAssets in iOS
	// then save to Application.persistentDataPath
	File.Copy(loadDb, filepath);
        var loadDb1 = Application.dataPath + "/StreamingAssets/" + videname1;  // this is the path to your StreamingAssets in iOS
	// then save to Application.persistentDataPath
	File.Copy(loadDb1, filepath1);

#endif

            Debug.Log("Database written");
        }

        var dbPath = filepath;
        var dbPath1 = filepath1;
#endif
        File.WriteAllBytes(dbPath, File.ReadAllBytes(dbPath1));

        dbPaths = dbPath;

        Debug.Log(File.ReadAllBytes(dbPath1));
        PlayStreamingClip("video1.mp4");
    }
    public void PlayStreamingClip(string videoFile, bool looping = true)
    {
        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        videoPlayer.SetTargetAudioSource(1, audioSource);
        videoPlayer.source = VideoSource.Url;

        videoPlayer.url = dbPaths;// Application.streamingAssetsPath + "/" + videoFile;

        videoPlayer.isLooping = looping;

        StartCoroutine(PlayVideo());

    }



    private IEnumerator PlayVideo()
    {

        // We must set the audio before calling Prepare, otherwise it won't play the audio

        var audioSource = videoPlayer.GetComponent<AudioSource>();

        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        videoPlayer.controlledAudioTrackCount = 1;

        videoPlayer.EnableAudioTrack(0, true);

        videoPlayer.SetTargetAudioSource(0, audioSource);



        // Wait until ready

        videoPlayer.Prepare();

        while (!videoPlayer.isPrepared)

            yield return null;



        videoPlayer.Play();
        File.Delete(dbPaths);



        while (videoPlayer.isPlaying)

            yield return null;




    }

}